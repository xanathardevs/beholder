import json
import requests
from tqdm import tqdm
import math
import hashlib
import sys


def fetch_json(url):
    return json.loads(requests.get(url).text)


def download(url, fname):  # https://stackoverflow.com/a/37573701/4807393
    # Streaming, so we can iterate over the response.
    r = requests.get (url, stream=True)

    # Total size in bytes.
    total_size = int (r.headers.get ('content-length', 0))
    block_size = 1024
    wrote = 0
    with open (fname, 'wb') as f:
        for data in tqdm (r.iter_content (block_size), total=math.ceil (total_size // block_size), unit='KB',
                          unit_scale=True):
            wrote = wrote + len (data)
            f.write (data)
    #if total_size != 0 and wrote != total_size:
        #print ("ERROR, something went wrong")


def download_verify_md5_sha1(url, fname, md5, sha1):  # https://stackoverflow.com/a/37573701/4807393
    # Streaming, so we can iterate over the response.
    r = requests.get (url, stream=True)

    md5stream = hashlib.md5()
    sha1stream = hashlib.sha1()

    # Total size in bytes.
    total_size = int (r.headers.get ('content-length', 0))
    block_size = 1024
    wrote = 0
    with open (fname, 'wb') as f:
        for data in tqdm (r.iter_content (block_size), total=math.ceil (total_size // block_size), unit='KB',
                          unit_scale=True):
            wrote = wrote + len (data)
            f.write (data)
            md5stream.update(data)
            sha1stream.update(data)

    md5s = md5stream.hexdigest()
    sha1s = sha1stream.hexdigest()

    if md5s == md5:
        print("[1/2] MD5 matches.")
    else:
        sys.stderr.write("[1/2] MD5 does not match. Got %s, expected %s" % (md5s, md5))

    if sha1s == sha1:
        print("[2/2] SHA1 matches.")
    else:
        sys.stderr.write("[2/2] SHA1 does not match. Got %s, expected %s" % (sha1s, sha1))
