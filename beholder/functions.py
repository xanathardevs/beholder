import beholder.fetch
import sys
import textwrap
import platform
import os
import json
import colorama
import zipfile
import shutil
colorama.init()
sys.stderr.write(colorama.Fore.RED)


def gunzip(name, out):
    zip_ref = zipfile.ZipFile (name, 'r')
    zip_ref.extractall (out)
    zip_ref.close ()


def parse_path(i):
    return os.path.join(beholder.DATA_DIR, "site-packages", i["name"])


def get_upstream(url):
    return beholder.fetch.fetch_json(url)


def lst(params, upstream_obj):
    search = params[0] if len(params) else ""
    if search:
        print("Searching for %s" % search)

    for j in upstream_obj["packages"]:
        i = upstream_obj["packages"][j]
        if search in i["name"]:
            print("[+] Package %s=%s: %s" % (i["name"].replace(search,
                                                               colorama.Fore.RED + search + colorama.Style.RESET_ALL),
                                             i["version"],
                                             i["description"] if i["description"] else "[No description]"))


def install(params, upstream_obj):
    with open(os.path.join(beholder.DATA_DIR, 'inst')) as f:
        instpkg = json.loads(f.read())

    packages_to_install = []
    names = []
    errs = False
    for i in params:
        if i not in upstream_obj["packages"]:
            sys.stderr.write("Could not find package %s!" % i)
            errs = True

        package_to_install = upstream_obj["packages"][i]
        if package_to_install not in packages_to_install:
            packages_to_install.append(package_to_install)
            names.append(i)
            print("Installing package %s" % i)

    if errs:
        exit(1)

    for i in packages_to_install:
        new = i["depends"]
        for j in new:
            if upstream_obj["packages"][j] not in packages_to_install:
                packages_to_install.append(upstream_obj["packages"][j])
                names.append(j)
                print("Adding required package %s" % j)

    print("Installing packages %s" % ', '.join(names))
    print("Continue? [Yn] ", end='')
    if input() == 'n':
        exit(0)

    for i in packages_to_install:
        if i["name"] + "==" + i["version"] not in instpkg["installed"]:
            beholder.fetch.download_verify_md5_sha1(i["upstream"], i["name"] + ".zip",
                                                    i["checksum"]["MD5"],
                                                    i["checksum"]["SHA1"])
            gunzip (i["name"] + ".zip", parse_path(i))
            instpkg["installed"].append(i["name"] + '==' + i["version"])
        else:
            print("Already installed %s==%s!" % (i["name"], i["version"]))

    with open(os.path.join(beholder.DATA_DIR, 'inst'), 'w') as w:
        w.write(json.dumps(instpkg))


def uninstall(params, upstream_obj):
    with open(os.path.join(beholder.DATA_DIR, 'inst')) as f:
        instpkg = json.loads(f.read())

    err = False
    for i in params:
        if i in instpkg["installed"]:
            instpkg["installed"].remove(i)
            shutil.rmtree(os.path.join(beholder.DATA_DIR, 'site-packages', i))
        else:
            sys.stderr.write("%s is not installed!\n" % i)
            err = True

    if err:
        exit(1)

    with open(os.path.join(beholder.DATA_DIR, 'inst'), 'w') as inst:
        inst.write(instpkg)


def config():
    pass


def version():
    print("Beholder version %s" % beholder.VERSION)


def sys_():
    cfg = {
        "version": sys.version.replace('\n', ''),
        "bv": beholder.VERSION,
        "os": platform.platform(),
        "xan": os.environ.get("XANATHAR_VERSION"),
    }
    print(textwrap.dedent("""
    Python {version}
    Beholder {bv}
    OS {os}
    Xanathar version {xan}""".format(**cfg)[1:]))


def behold():
    with open('secret') as secret:
        print(secret.read())
