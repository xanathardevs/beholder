import json
import sys
import textwrap
import os
try:
    import beholder.fetch
    import beholder.functions
except ImportError as e:
    p = os.path.abspath(__file__)
    sys.path.insert(0, os.path.dirname(os.path.dirname(p)))
    import beholder.fetch
    import beholder.functions


VERSION = "a0.0.1"
DATA_DIR = '/usr/lib/beholder/'


def print_help():
    print(textwrap.dedent("""
    Beholder (Xanathar package manager). Commands:
    + list [term]? : lists packages (optionally matching [term])
    + install [package] : installs [package]
    + uninstall [package] : uninstalls [package]
    + config : opens interactive config modifier
    + version : prints version
    + sys : prints relevant system information (use for bug reports)
    """)[1:])


def switch(command, parameters, upstream):
    up = beholder.functions.get_upstream(upstream)

    if command == "list":
        beholder.functions.lst(parameters, up)
    elif command == "install":
        beholder.functions.install(parameters, up)
    elif command == "uninstall":
        beholder.functions.uninstall(parameters, up)
    elif command == "config":
        beholder.functions.config()
    elif command == "version":
        beholder.functions.version()
    elif command == "sys":
        beholder.functions.sys_()
    elif command == "behold":
        beholder.functions.behold()
    else:
        print_help()


if __name__ == "__main__":
    lock_file = open(os.path.join(DATA_DIR, 'lock'), 'w')
    lock_file.write(str(os.getpid()))
    with open("constants.json") as constants_file:
        constants = json.loads(constants_file.read())

    if len(sys.argv) == 1:
        print_help()
    elif len(sys.argv) > 1:
        switch(sys.argv[1], sys.argv[2:] if len(sys.argv) > 2 else [], constants["UPSTREAM_URL"])

    lock_file.close()
